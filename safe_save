#!/usr/bin/python3
#+
# Example use of python_linuxfs: create a file which does not
# actually appear in the filesystem until it has been fully
# written and closed. Any unexpected termination before then
# causes it to just disappear. Invoke this script as follows:
#
#     safe_save [--definite] [--replace] «filename» «text-line»
#
# where «filename» is the name of the file to create, and «text-line»
# is some text to put in the file. The options are
#
#     --definite
#         -- do a regular file create instead of a tentative create,
#            just for comparison.
#            Incompatible with --safe-replace.
#     --replace
#         -- replace any existing file named «filename». Actually
#            the existing file is renamed to «filename»-old.
#            Otherwise it is an error if the file already exists.
#            For a tentative create, this error is not reported until
#            the new file is to be given the same name. (Consider that
#            a feature of this sample code.)
#            Incompatible with --safe-replace.
#     --safe-replace
#         -- replace any existing file named «filename» in a safe
#            fashion, so that in case of error the original file
#            remains under its original name.
#            Incompatible with --definite or --replace.
#
# Copyright 2022 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>. This
# script is licensed CC0
# <https://creativecommons.org/publicdomain/zero/1.0/>; do with it
# what you will.
#-

import sys
import os
import time
import getopt
import linuxfs as lx

tentative = True
replace = False
safe_replace = False
opts, args = getopt.getopt \
  (
    sys.argv[1:],
    "",
    ["definite", "replace", "safe-replace"]
  )
for keyword, value in opts :
    if keyword == "--definite" :
        tentative = False
    elif keyword == "--replace" :
        replace = True
    elif keyword == "--safe-replace" :
        safe_replace = True
    #end if
#end for
if (replace or not tentative) and safe_replace :
    raise getopt.GetoptError("--safe-replace is not compatible with --replace or --definite")
#end if
if len(args) != 2 :
    raise getopt.GetoptError("expecting just 2 args, the «filename» and the «text-line»")
#end if
filepath, textline = args
filemode = 0o700

if tentative :
    outfd = lx.open_at \
      (
        dirfd = lx.AT_FDCWD,
        pathname = os.path.split(os.path.abspath(filepath))[0],
        flags = os.O_RDWR | os.O_TMPFILE,
        mode = filemode
      )
else :
    if replace :
        try :
            os.rename(filepath, filepath + "-old")
        except FileNotFoundError :
            pass
        #end try
    #end if
    outfd = lx.open_at \
      (
        dirfd = lx.AT_FDCWD,
        pathname = filepath,
        flags = os.O_CREAT | os.O_RDWR | os.O_EXCL,
        mode = filemode
      )
#end if
os.write(outfd, (textline + "\n").encode())
if tentative :
    sys.stderr.write("file is currently open at /proc/%d/fd/%d\n" % (os.getpid(), outfd))
    time.sleep(10) # let user examine symlinks if they should be so inclined
    if safe_replace :
        tempfilepath = filepath + "-new"
        lx.save_tmpfile(outfd, tempfilepath)
        try :
            lx.rename_at(lx.AT_FDCWD, tempfilepath, lx.AT_FDCWD, filepath, lx.RENAME_EXCHANGE)
        except FileNotFoundError :
            os.rename(tempfilepath, filepath)
        else :
            os.unlink(tempfilepath)
        #end try
    else :
        if replace :
            try :
                os.rename(filepath, filepath + "-old")
            except FileNotFoundError :
                pass
            #end try
        #end if
        lx.save_tmpfile(outfd, filepath)
    #end if
#end if
os.close(outfd)
